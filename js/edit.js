((Drupal, drupalSettings) => {
  // Get nid.
  const nid = drupalSettings.ecoindex.nid;

  // Get EcoIndex field_name.
  const field = drupalSettings.ecoindex.field;

  // Get Current langcode.
  const language = drupalSettings.path.currentLanguage;

  // Get score and save it.
  const score = localStorage.getItem(`ecoindex.score.${language}.${nid}`);
  if (score) {
    document.getElementById(
      `edit-${field.replaceAll('_', '-')}-0-score`,
    ).value = score;
    localStorage.removeItem(`ecoindex.score.${language}.${nid}`);
  }

  // Get grade and save it.
  const grade = localStorage.getItem(`ecoindex.grade.${language}.${nid}`);
  if (grade) {
    document.getElementById(
      `edit-${field.replaceAll('_', '-')}-0-grade`,
    ).value = grade;
    localStorage.removeItem(`ecoindex.grade.${language}.${nid}`);
  }
})(Drupal, drupalSettings);
